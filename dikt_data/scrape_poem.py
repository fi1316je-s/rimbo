import requests
from bs4 import BeautifulSoup
import regex as re

base_url = "https://svenskadikter.com/"
category = "Kategori:2000-talets_f%C3%B6rfattare"

resp = requests.get(base_url+category)
soup = BeautifulSoup(resp.content, "html.parser")
author_links = soup.find_all("a", {"href": re.compile("/Kategori.*")})
for link in author_links:
    author_resp = requests.get(base_url + category + link["href"])
    author_soup = BeautifulSoup(author_resp.content, "html.parser")
    mw_content_div = soup.find("div", {"class": "mw-content-ltr"})
    poem_links = mw_content_div.find_all("a")
    for poem_link in poem_links:
        poem_resp = requests.get(base_url + poem_link["href"])
        poem_soup = BeautifulSoup(poem_resp, "html.parser")
        poem_text = poem_soup.find("div", {"class": "dikt"}).find('p').text()
        print(poem_text)
