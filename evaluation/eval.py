from ast import arg
import pickle
import argparse
import string

parser = argparse.ArgumentParser(description="Rhyming couplet evaluator")

parser.add_argument('filename', help="Filename of pickled couplets to evaluate")

args = parser.parse_args()

with open(args.filename, 'rb') as file:
    data = pickle.load(file)

def rate(i,n,couplet):
    print(f"\nevaluating {i+1}/{n}")
    print(100*"-")
    print(couplet)
    print(100*"-")
    rating = input("1:trash\n2:almost there\n3:acceptable\n")
    if rating in ["1","2","3"]:
        return rating
    else:
        return None
    

scores = []
for i,couplet in enumerate(data):
    score = rate(i,len(data),couplet)
    while score == None:
        print("\nonly input 1, 2, or 3")
        score = rate(i,len(data),couplet)
    scores.append(score)

with open("result_"+args.filename, 'wb') as file:
    print(f"Saving scores as result_{args.filename}")
    pickle.dump(scores, file)


