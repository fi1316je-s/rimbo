using HTTP, Gumbo, Cascadia, Serialization
import JSON, ProgressMeter


const BASEURL = "https://dblex.com/rimlexikon/index.php?rim="

const wordlist = [replace(word, " "=>"+") for word in readlines("svenska-ord.txt")]

function extractrhyme(x)
    tdchildren = children(children(x)[3])
    return isempty(tdchildren) ? nothing : tdchildren[1].text
end

function findrhymes(word::String)
    response = parsehtml(String(HTTP.get(BASEURL*word; connection_limit=100, pipeline_limit=100, connect_timeout=1, readtimeout=1, retries=1).body))
    trs = eachmatch(Selector(".output tbody tr"), response.root)
    rhymes::Vector{String} = filter(!=(nothing), extractrhyme.(trs))
    return rhymes
end

function rhymedatabase(; workers=10)
    rhymesets = Vector{Set{String}}()
    rhymeindices = Dict{String, UInt64}()
    p = ProgressMeter.Progress(length(wordlist); showspeed=true)
    for words in Iterators.partition(wordlist, workers)
        rhymestasks = []
        for word in words
            if !in(word, keys(rhymeindices))
                push!(rhymestasks, (word, @async(findrhymes(word))))
            end
            ProgressMeter.next!(p)
        end
        for (word,task) in rhymestasks
            rhymes = String[]
            try
                rhymes = fetch(task)
            catch e
                while true
                    try 
                        rhymes = findrhymes(word)
                        break
                    catch e
                    end
                end
            end
            if !isempty(rhymes)
                newindex = length(rhymesets) + 1
                i = newindex
                for rhyme in rhymes
                    if rhyme in keys(rhymeindices)
                        i = rhymeindices[rhyme]
                        break
                    end
                end
                if i < newindex
                    union!(rhymesets[i], rhymes)
                else
                    push!(rhymesets, Set(rhymes))
                end
                merge!(rhymeindices, Dict(rhyme=>i for rhyme in rhymes))
            end
            
        end
    end
    rhymedb = Dict{String, Set{String}}()
    for (word, i) in rhymeindices
        rhymedb[word] = rhymesets[i]
    end
    return rhymedb, rhymesets, rhymeindices
end

db = rhymedatabase(workers=10)

using BSON

BSON.write("rhymedb.bson", db)

db = BSON.load("rhymedb.bson")[:db]

function split_rhymedb(db)
    rhymesets = Vector{Set{String}}()
    rhymeindices = Dict{String, UInt64}()
    ProgressMeter.@showprogress for word in keys(db)
        success = false
        for (i, set) in enumerate(rhymesets)
            if word in set
                rhymeindices[word] = i
                success = true
                break
            end
        end
        if !success
            push!(rhymesets, db[word])
            rhymeindices[word] = length(rhymesets)
        end
    end
    return rhymesets, rhymeindices
end

rhymesets, rhymeindices = split_rhymedb(db)

using JSON3
write("rhymedb_split.json", JSON3.write(Dict("rhymesets"=>rhymesets, "rhymeindices"=>rhymeindices)))